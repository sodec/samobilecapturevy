//
//  NFCWrapper.swift
//  NFCTest
//
//  Created by Erkan SIRIN on 8.03.2021.
//

import Foundation
import UIKit
import CoreNFC

@objc public protocol SAReadDelegate{
    
    func didReadDone(saChipData: SAChipData)
    func didReadChipDoneWithError(error: NSError)
    
}

@available(iOS 13, *)
@objcMembers public class NFCWrapper : NSObject ,SAChipDelegate{
  
    
//    public func didReadChipLog(logString: String) {
//        delegate?.didReadChipLog(logString: logString)
//    }
    
    public func didReadChipDone(saChipData: SAChipData) {
        
    }
    

    
    public func didReadChipDoneWithError(error: NSError) {
        delegate?.didReadChipDoneWithError(error: error)
    }
    
    public func didReadChipCancel() {
        delegate?.didReadChipCancel()
    }
    
    public var delegate :SAChipDelegate?
    let passportReader = PassportReader()
    
    public var nfcReadingTittle = "Lütfen kartınızı haraket ettirmeyin"
    @objc public func setParam(nfcReadingTittle:NSString){
        self.nfcReadingTittle = "\(nfcReadingTittle)"
    }
    
    public var nfcInfoTittle = "NFC Chip özelliği olan dökümanı cihazın üst kısmına tutunuz okuma bitene kadar çekmeyiniz."
    @objc public func setParam(nfcInfoTittle:NSString){
        self.nfcInfoTittle = "\(nfcInfoTittle)"
    }
    
    public var validateDocumentTittle = "Döküman doğrulanıyor"
    @objc public func setParam(validateDocumentTittle:NSString){
        self.validateDocumentTittle = "\(validateDocumentTittle)"
    }
    
    public var invalidTagTittle = "Geçersiz tag."
    @objc public func setParam(invalidTagTittle:NSString){
        self.invalidTagTittle = "\(invalidTagTittle)"
    }
  
    public var multiTagTittle = "Geçersiz multi tag."
    @objc public func setParam(multiTagTittle:NSString){
        self.multiTagTittle = "\(multiTagTittle)"
    }
    
    public var connectionErrorTittle = "Bağlantı hatası. Lütfen tekrar deneyin."
    @objc public func setParam(connectionErrorTittle:NSString){
        self.connectionErrorTittle = "\(connectionErrorTittle)"
    }
    
    public var mrzErrorTittle = "MRZ anahtar uyumsuzdur."
    @objc public func setParam(mrzErrorTittle:NSString){
        self.mrzErrorTittle = "\(mrzErrorTittle)"
    }
    
    
    public var readingErrorTittle = "Döküman okuma hatası."
    @objc public func setParam(readingErrorTittle:NSString){
        self.readingErrorTittle = "\(readingErrorTittle)"
    }
    
    public var readingSuccessTittle = "Döküman okuma başarılı."
    @objc public func setParam(readingSuccessTittle:NSString){
        self.readingSuccessTittle = "\(readingSuccessTittle)"
    }
    
    public var corruptedDataTittle = "Part of returned data may be corrupted"
    @objc public func setParam(corruptedDataTittle:NSString){
        self.corruptedDataTittle = "\(corruptedDataTittle)"
    }
    
    public var fileInvalidatedTittle = "file invalidated"
    @objc public func setParam(fileInvalidatedTittle:NSString){
        self.fileInvalidatedTittle = "\(fileInvalidatedTittle)"
    }
    
    public var fCIErrorTittle = "FCI not formatted according to ISO7816-4 section 5.1.5"
    @objc public func setParam(fCIErrorTittle:NSString){
        self.fCIErrorTittle = "\(fCIErrorTittle)"
    }
    
    
    public var transmissionErrorTittle = "Secured Transmission not supported"
    @objc public func setParam(transmissionErrorTittle:NSString){
        self.transmissionErrorTittle = "\(transmissionErrorTittle)"
    }
    
    public var memoryErrorTittle = "Memory failure"
    @objc public func setParam(memoryErrorTittle:NSString){
        self.memoryErrorTittle = "\(memoryErrorTittle)"
    }
    
    public var commandErrorTittle = "Invalid command"
    @objc public func setParam(commandErrorTittle:NSString){
        self.commandErrorTittle = "\(commandErrorTittle)"
    }
    
    public var authenticationErrorTittle = "Authentication error"
    @objc public func setParam(authenticationErrorTittle:NSString){
        self.authenticationErrorTittle = "\(authenticationErrorTittle)"
    }
    
    public var invalidDataErrorTittle = "Invalid data"
    @objc public func setParam(invalidDataErrorTittle:NSString){
        self.invalidDataErrorTittle = "\(invalidDataErrorTittle)"
    }
    
    
    
    @objc public func readNFC(idNumber:String,dateOfBirth:String,expiryDate:String,certificateFileURL:URL,delegate:SAReadDelegate){
        print("readNFC")
       
        
        let passportNrChksum = calcCheckSum(idNumber)
        let dateOfBirthChksum = calcCheckSum(dateOfBirth)
        let expiryDateChksum = calcCheckSum(expiryDate)
        
        
        let mrzKey = "\(idNumber)\(passportNrChksum)\(dateOfBirth)\(dateOfBirthChksum)\(expiryDate)\(expiryDateChksum)"

      
       // let masterListURL = Bundle.main.url(forResource: "masterList", withExtension: ".pem")!
        passportReader.setMasterListURL( certificateFileURL )
        //self.delegate?.didReadChipLog(logString:"mrz for read session : \(mrzKey)")
        passportReader.readPassport(delegate: self,mrzKey: mrzKey, customDisplayMessage: { (displayMessage) in
            switch displayMessage {
            
            
            case .successfulRead:
                return "\(self.readingSuccessTittle)"
            
            case .readingDataGroupProgress(let dataGroup, let progress):
                let progressString = self.handleProgress(percentualProgress: progress)
                
                var progressS = ""
                if dataGroup == .COM {
                    progressS = self.handleProgress(percentualProgress: 10)
                }
                if dataGroup == .DG14 {
                    progressS = self.handleProgress(percentualProgress: 20)
                }
                if dataGroup == .SOD {
                    progressS = self.handleProgress(percentualProgress: 30)
                }
                if dataGroup == .DG1 {
                    progressS = self.handleProgress(percentualProgress: 40)
                }
                if dataGroup == .DG2 {
                    progressS = self.handleProgress(percentualProgress: 50)
                }
                if dataGroup == .DG5 {
                    progressS = self.handleProgress(percentualProgress: 60)
                }
                if dataGroup == .DG11 {
                    progressS = self.handleProgress(percentualProgress: 70)
                }
                if dataGroup == .DG12 {
                    progressS = self.handleProgress(percentualProgress: 80)
                }
                
                if dataGroup == .DG15 {
                    progressS = self.handleProgress(percentualProgress: 100)
                }
   
                print("Okunan Veri: \(dataGroup).....\n\n\(progressString)")
                return "\(self.nfcReadingTittle)\n\n\(progressS)"
                
                case .requestPresentPassport:
                    return "\(self.nfcInfoTittle)"
                  
            case .error(let tagError):
                switch tagError {
                    case NFCPassportReaderError.TagNotValid:
                        return "\(self.invalidTagTittle)"
                    case NFCPassportReaderError.MoreThanOneTagFound:
                        return "\(self.multiTagTittle)"
                case NFCPassportReaderError.ConnectionError:
                    return "\(self.connectionErrorTittle)"
                    case NFCPassportReaderError.InvalidMRZKey:
                        return "\(self.mrzErrorTittle)"
                    case NFCPassportReaderError.ResponseError(let description, let sw1, let sw2):
                        
                        
                        

                        if description == "Part of returned data may be corrupted" {
                            return "\(self.corruptedDataTittle)"
                        }
                           if description == "End of file/record reached before reading Le bytes"{
                            return "\(self.corruptedDataTittle)"
                           }
                        if description == "Selected file invalidated"{
                            return "\(self.fileInvalidatedTittle)"
                            
                        }
                        if description == "FCI not formatted according to ISO7816-4 section 5.1.5"{
                            return "\(self.fCIErrorTittle)"
                        }

                        if description == "File filled up by the last write"{
                            return "\(self.corruptedDataTittle)"
                        }
                        if description == "Card Key not supported"{
                            return "\(self.mrzErrorTittle)"
                            
                        }
                        if description == "Reader Key not supported"{
                            return "\(self.mrzErrorTittle)"
                            
                        }
                        if description == "Plain transmission not supported"{
                            return "\(self.transmissionErrorTittle)"
                        }
                        if description == "Secured Transmission not supported"{
                            return "\(self.transmissionErrorTittle)"
                            
                        }
                        if description == "Volatile memory not available"{
                            return "\(self.memoryErrorTittle)"
                        }
                        if description == "Non Volatile memory not available"{
                            return "\(self.memoryErrorTittle)"
                        }
                        if description == "Key number not valid"{
                            return "\(self.memoryErrorTittle)"
                        }
                        if description == "Key length is not correct"{
                            return "\(self.memoryErrorTittle)"
                        }
                        if description == "Counter provided by X (valued from 0 to 15) (exact meaning depending on the command)"{
                            return "\(self.memoryErrorTittle)"
                        }
                     
                        if description == "Memory failure"{
                            return "\(self.memoryErrorTittle)"
                        }
                        if description == "Wrong length"{
                            return "\(self.memoryErrorTittle)"
                        }
                    
                        if description == "Logical channel not supported"{
                            return "\(self.transmissionErrorTittle)"
                        }
                        if description == "Secure messaging not supported"{
                            return "\(self.transmissionErrorTittle)"
                        }
                    
                        if description == "Command incompatible with file structure"{
                        return "\(self.commandErrorTittle)"
                        }

                        if description == "Authentication method blocked"{
                        return "\(self.authenticationErrorTittle)"
                        }
                        if description == "Referenced data invalidated"{
                        
                        }
                        if description == "Conditions of use not satisfied"{
                            return "\(self.commandErrorTittle)"
                        }
                        if description == "Command not allowed (no current EF)"{
                            return "\(self.commandErrorTittle)"
                        }
                        if description == "Expected SM data objects missing"{
                        return "\(self.invalidDataErrorTittle)"
                        }
                        if description == "SM data objects incorrect"{
                            return "\(self.invalidDataErrorTittle)"
                        }
                  
                        if description == "Incorrect parameters in the data field"{
                            return "\(self.invalidDataErrorTittle)"
                        }
                        if description == "Function not supported"{
                            return "\(self.commandErrorTittle)"
                        }
                        if description == "File not found"{
                            return "\(self.fileInvalidatedTittle)"
                        }
                        if description == "Record not found"{
                            return "\(self.readingErrorTittle)"
                        }
                        if description == "Not enough memory space in the file"{
                            return "\(self.memoryErrorTittle)"
                        }
                        if description == "Lc inconsistent with TLV structure"{
                            return "\(self.readingErrorTittle)"
                        }
                        if description == "Incorrect parameters P1-P2"{
                            return "\(self.commandErrorTittle)"
                        }
                        if description == "Lc inconsistent with P1-P2"{
                            return "\(self.commandErrorTittle)"
                        }
                        if description == "Referenced data not found"{
                            return "\(self.invalidDataErrorTittle)"
                        }
                        if description == "Wrong parameter(s) P1-P2]"{
                            return "\(self.commandErrorTittle)"
                        }
                        if description == "Instruction code not supported or invalid"{
                            return "\(self.readingErrorTittle)"
                        }
                        if description == "Class not supported"{
                            return "\(self.readingErrorTittle)"
                        }
                        if description == "No precise diagnosis"{
                            return "\(self.readingErrorTittle)"
                        }
                        if description == "Success"{
                        return "Başarılı"
                        }
                 
                        
                        if description == "No information given" {
                            return "\(self.readingErrorTittle)"
                        }
                        
                        if description == "Güvenlik gereksinimleri eksik MRZ anahtarınızı kontrol edin." {
                            return "\(self.mrzErrorTittle)"
                        }
                        
                        
                        return "\(self.readingErrorTittle) \(description) - (0x\(sw1), 0x\(sw2)"
                    default:
                        return "\(self.readingErrorTittle)"
                }
                
            case .authenticatingWithPassport(let progress):
                let progressString = self.handleProgress(percentualProgress: progress)
                return "\(self.validateDocumentTittle)"
                default:
                    print("Chip özelliği olan dökümanı cihazın üst kısmına tutunuz okuma bitene kadar çekmeyiniz.")
                    // Return nil for all other messages so we use the provided default
                    return nil
            }
        }, completed: { (passport, error) in
            if let passport = passport {
                // All good, we got a passport

                DispatchQueue.main.async { [self] in
                   
                    print("passport.placeOfBirth : ",passport.placeOfBirth)
//                    let passSgtr = "\(passport.documentSigningCertificate?.getSubjectName() ?? "X")"
//
//                    let utf8Data = Data(passSgtr.utf8)
//                    let latin1 = String(data: utf8Data, encoding: .utf8)!
//                    print("latin1 : ",latin1)
//
//                    let str = String(describing: passSgtr.cString(using: String.Encoding.utf8))
//                    print("passport.lastName : ",str)
//                    print("passport.documentSigningCertificate : ",(passport.documentSigningCertificate?.getIssuerName())! as String)
//                    print("passport.getSubjectName : ",passport.documentSigningCertificate?.getSubjectName())
                    //print("custodyInfo : ",passport.custodyInfo)
                  
                 var firstName = ""
                    var lastName = ""
                    let fullNameArray = passport.fullName.components(separatedBy: "<<")
                    if fullNameArray.count == 2 {
                        firstName = fullNameArray[1]
                        lastName = fullNameArray[0]
                    }else{
                        firstName = passport.firstName
                        lastName = passport.lastName
                    }
                        let chipData = SAChipData(activeAuthenticationSupported: passport.activeAuthenticationSupported, signatureImage: passport.signatureImage, passportImage: passport.passportImage, activeAuthenticationPassed: passport.activeAuthenticationPassed, passportDataNotTampered: passport.passportDataNotTampered, documentSigningCertificateVerified: passport.documentSigningCertificateVerified, passportCorrectlySigned: passport.passportCorrectlySigned, firstName: firstName as NSString, lastName: lastName as NSString, fullName: "\(firstName) \(lastName)" as NSString, nationality: passport.nationality as NSString, gender: passport.gender as NSString, dateOfBirth: passport.dateOfBirth as NSString,birthPlace:passport.placeOfBirth as NSString, documentExpiryDate: passport.documentExpiryDate as NSString, issuingAuthority: passport.issuingAuthority as NSString, documentNumber: passport.documentNumber as NSString, personalNumber: passport.personalNumber as NSString, documentSubType: passport.documentSubType as NSString, documentType: passport.documentType as NSString)
                        

                        delegate.didReadDone(saChipData: chipData)
                 
                    
                    
                    
                  
                    
         
                   
                    
                    //
                    
                }

            } else {
                
                print("\(error?.localizedDescription ?? "Unknown error")")
                if error != nil {
                    delegate.didReadChipDoneWithError(error: error! as NSError)
                }else{
                    delegate.didReadChipDoneWithError(error: NSError(domain: "sodec nfc reader", code: 44, userInfo: ["hata:":"Bilinmeyen hata"]))
                }
                
                
            }
        })
    }
    
    
    func calcCheckSum( _ checkString : String ) -> Int {
        let characterDict  = ["0" : "0", "1" : "1", "2" : "2", "3" : "3", "4" : "4", "5" : "5", "6" : "6", "7" : "7", "8" : "8", "9" : "9", "<" : "0", " " : "0", "A" : "10", "B" : "11", "C" : "12", "D" : "13", "E" : "14", "F" : "15", "G" : "16", "H" : "17", "I" : "18", "J" : "19", "K" : "20", "L" : "21", "M" : "22", "N" : "23", "O" : "24", "P" : "25", "Q" : "26", "R" : "27", "S" : "28","T" : "29", "U" : "30", "V" : "31", "W" : "32", "X" : "33", "Y" : "34", "Z" : "35"]
        
        var sum = 0
        var m = 0
        let multipliers : [Int] = [7, 3, 1]
        for c in checkString {
            guard let lookup = characterDict["\(c)"],
                let number = Int(lookup) else { return 0 }
            let product = number * multipliers[m]
            sum += product
            m = (m+1) % 3
        }
        
        return (sum % 10)
    }
    
    
    func handleProgress(percentualProgress: Int) -> String {
        print("percentualProgress Wrapper: ",percentualProgress)
        let p = (percentualProgress/20)
        let full = String(repeating: "🟢 ", count: p)
        let empty = String(repeating: "⚪️ ", count: 5-p)
        return "\(full)\(empty)"
    }
    
}






