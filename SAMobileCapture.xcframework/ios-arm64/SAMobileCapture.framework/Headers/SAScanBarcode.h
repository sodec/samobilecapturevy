/*
* Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
*
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* Written by Hasan Dertli <hasan.dertli@sodecapps.com>
*
* Sodec Apps Bilisim Teknolojileri
* http://sodecapps.com
* support@sodecapps.com
*/

#import "SADefineCapture.h"
#import "SABarcodeFormats.h"
#import "SABarcode.h"

@class SAScanBarcode;

@protocol SAScanBarcodeDelegate <NSObject>

@required
- (void)scanBarcodeDidCancel:(SAScanBarcode *)controller;
- (void)scanBarcodeDidDone:(SAScanBarcode *)controller withBarcodes:(NSArray<SABarcode *> *)barcodes;
- (BOOL)scanBarcodeOnValidate:(NSString *)barcodeValue;

@end

@interface SAScanBarcode : UIViewController
{
    id<SAScanBarcodeDelegate> __unsafe_unretained delegate;
}

@property (unsafe_unretained) id<SAScanBarcodeDelegate> delegate;
@property (strong, nonatomic, readwrite) NSString *navBarTitle;
@property (strong, nonatomic, readwrite) UIColor *previewBackgroundColor;
@property (nonatomic, assign) SACaptureQuality captureQaulity;
@property (strong, nonatomic, readwrite) SABarcodeFormats *validBarcodeFormats;
@property (strong, nonatomic, readwrite) NSString *notificationMessage;

@end
