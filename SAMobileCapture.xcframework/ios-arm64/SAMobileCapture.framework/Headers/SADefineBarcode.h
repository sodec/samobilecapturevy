/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

typedef NS_OPTIONS(NSInteger, SABarcodeFormat)
{
    SABarcodeFormatEAN8 = 0x0001,
    SABarcodeFormatEAN13 = 0x0002,
    SABarcodeFormatUPCA = 0x0004,
    SABarcodeFormatUPCE = 0x0008,
    SABarcodeFormatCode39 = 0x0010,
    SABarcodeFormatCode93 = 0x0020,
    SABarcodeFormatCode128 = 0x0040,
    SABarcodeFormatITF = 0x0080,
    SABarcodeFormatCodaBar = 0x0100,
    SABarcodeFormatQRCode = 0x0200,
    SABarcodeFormatDataMatrix = 0x0400,
    SABarcodeFormatPDF417 = 0x0800,
    SABarcodeFormatAztec = 0x1000,
};
